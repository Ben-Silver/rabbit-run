﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /* Public Variables */
    public float jumpSpeed = 50;

    /* Private Variables */
    private Rigidbody2D rb;
    private bool jumping;

	// Use this for initialization
	void Start()
    {
        // Get the rigid body on the GameObject
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate()
    {
        if (!FindObjectOfType<GameManager>().gameIsPlaying)
        {
            rb.velocity = Vector2.zero;
        }
        
        // Make the player jump
        Jump();
	}

    /// <summary>
    /// Function to make the player jump
    /// </summary>
    public void Jump()
    {
        // Set jumping to true/false based on the player's vertical veloctity
        jumping = (rb.velocity.y != 0) ? true : false;

        // Check for input and check whether the player is currently jumping
        if (Input.GetButtonDown("Fire1") && !jumping)
        {
            // Jump
            rb.AddForce(Vector2.up * jumpSpeed);
        }
    }
}
