﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
	/* Public Variables */
	[Header("Score Elements")]
	public LeaderboardManager leaderboard;
	public float score;

	[Header("Controller Elements")]
	public float countdown = 3;
	public bool gameIsPlaying;

	[Header("UI Elements")]
	public Text scoreText;
	public Text highScoreText;
	public TextMeshProUGUI gameOverScoreText;
	public GameObject gameOverHighScoreText;

	#region Singleton
	/*public static GameManager instance;

	void Awake()
	{
		// Check to see if there is already an instance of the gameObject in the scene
		if (instance == null)
		{
			// Set the instance to this gameObject
			instance = this;
		}
		else
		{
			// Remove unwanted instances
			Destroy(gameObject);
		}

		// Make sure the gameObject isn't destroyed when a new level is loaded
		DontDestroyOnLoad(gameObject);
	}
	*/
	#endregion

	// Use this for initialization
	void Start()
	{
		// If no leaderboard is assigned
		if (!leaderboard)
		{
			// Find the leaderboard in the scene
			leaderboard = FindObjectOfType<LeaderboardManager>();
		}

		// Display the high score in the UI
		highScoreText.text = "High Score: " + leaderboard.leaderboard[leaderboard.leaderboard.Count - 1].ToString();
	}
	
	// Update is called once per frame
	void Update()
	{
		// Update the countdown
		UpdateCountdown();

		// Update the game state
		UpdateGameState();
	}

	/// <summary>
	/// Updates the countdown before the start of the game
	/// </summary>
	public void UpdateCountdown()
	{
		// Count down the time until the game starts
		countdown -= (!gameIsPlaying) ? Time.deltaTime : 3;

		// If the countdown reaches 0
		if (countdown <= 0)
		{
			// Start the game
			gameIsPlaying = true;

			// Reset countdown
			countdown = 3;
		}
	}

	/// <summary>
	/// Update the state of the game
	/// </summary>
	public void UpdateGameState()
	{
		// If the game is playing
		if (gameIsPlaying)
		{
			// Increase the score as time progresses
			score += Time.deltaTime;

			// Show the score as an int in the UI
			scoreText.text = "Score: " + Mathf.RoundToInt(score).ToString();

			// If the current score exceeds the high score
			if (score >= leaderboard.leaderboard[leaderboard.leaderboard.Count - 1])
			{
				// Update the high score text with the current score
				highScoreText.text = "High Score: " + Mathf.RoundToInt(score).ToString();
			}
		}
		else
		{
			// Hide the score text
			scoreText.text = "";

			// Hide the high score text
			highScoreText.text = "";
		}
	}
}
