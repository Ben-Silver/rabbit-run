﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
	/* Public Variables */
	public int platformSpeed = 12;

	/* Private Variables */
	private float lifeTimer = 0;
	private float lifeSpan = 3.5f;

	// Use this for initialization
	void Start()
	{
		
	}
	
	// Update is called once per frame
	void Update()
	{
		// If the game is playing
		if (FindObjectOfType<GameManager>().gameIsPlaying)
		{
			// Move the platform towards the player
			transform.Translate(new Vector2(-platformSpeed * Time.deltaTime, 0));
			
			// Increase the timer in real time
			lifeTimer += Time.deltaTime;

			// If the timer reaches its maximum
			if (lifeTimer >= lifeSpan)
			{
				// Destroy the platform
				Destroy(gameObject);
			}
		}
	}
}
