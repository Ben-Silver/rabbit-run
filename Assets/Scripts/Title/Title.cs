﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Title : MonoBehaviour
{
    /* Public Variables */
    [Header("Leaderboard Elements")]
    public LeaderboardManager leaderboard;
    public TextMeshProUGUI leaderboardText;
    
    [Header("UI Elements")]
    public GameObject titleWindow;
    public GameObject leaderboardWindow;
    public GameObject settingsWindow;

    // Start is called before the first frame update
    void Start()
    {
        // If no leaderboard is assigned
        if (!leaderboard)
        {
            // Find the leaderboard
            leaderboard = FindObjectOfType<LeaderboardManager>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Called when a button in the UI is pressed
    /// </summary>
    public void OnButtonPressed(string buttonName)
    {
        // If the button has the "Run!" string
        if (buttonName.Equals("Run!"))
        {
            // Load the game
            SceneManager.LoadScene("Game");
        }
        // If the button has the "Leaderboard" string
        else if (buttonName.Equals("Leaderboard"))
        {
            // Show the leaderboard window
            leaderboardWindow.SetActive(true);

            // Hide the title window
            titleWindow.SetActive(false);
    
            // Set the intital leaderboard text
            leaderboardText.text = "Leaderboard:\n";

            // If there are 5 or more high scores stored
            if (leaderboard.leaderboard.Count >= 5)
            {
                // Loop through the top 5 high scores
                for (int i = leaderboard.leaderboard.Count - 1; i > leaderboard.leaderboard.Count - 6; i--)
                {
                    // Append the score at i to the text
                    leaderboardText.text += " " + (leaderboard.leaderboard.Count - i).ToString() + ": " +
                                            leaderboard.leaderboard[i].ToString() + "\n";
                }
            }
            else if (leaderboard.leaderboard.Count >= 4)
            {
                // Loop through the top 4 high scores
                for (int i = leaderboard.leaderboard.Count - 1; i > leaderboard.leaderboard.Count - 5; i--)
                {
                    // Append the score at i to the text
                    leaderboardText.text += " " + (leaderboard.leaderboard.Count - i).ToString() + ": " +
                                            leaderboard.leaderboard[i].ToString() + "\n";
                }
            }
            else if (leaderboard.leaderboard.Count >= 3)
            {
                // Loop through the top 3 high scores
                for (int i = leaderboard.leaderboard.Count - 1; i > leaderboard.leaderboard.Count - 4; i--)
                {
                    // Append the score at i to the text
                    leaderboardText.text += " " + (leaderboard.leaderboard.Count - i).ToString() + ": " +
                                            leaderboard.leaderboard[i].ToString() + "\n";
                }
            }
            else if (leaderboard.leaderboard.Count >= 2)
            {
                // Loop through the top 2 high scores
                for (int i = leaderboard.leaderboard.Count - 1; i > leaderboard.leaderboard.Count - 3; i--)
                {
                    // Append the score at i to the text
                    leaderboardText.text += " " + (leaderboard.leaderboard.Count - i).ToString() + ": " +
                                            leaderboard.leaderboard[i].ToString() + "\n";
                }
            }
            else if (leaderboard.leaderboard.Count >= 1)
            {
                // Loop through the top 1 high scores
                for (int i = leaderboard.leaderboard.Count - 1; i > leaderboard.leaderboard.Count - 2; i--)
                {
                    // Append the score at i to the text
                    leaderboardText.text += " " + (leaderboard.leaderboard.Count - i).ToString() + ": " +
                                            leaderboard.leaderboard[i].ToString() + "\n";
                }
            }
        }
        // If the button has the "Settings" string
        else if (buttonName.Equals("Settings"))
        {
            // Show the settings window
            settingsWindow.SetActive(true);

            // Hide the title window
            titleWindow.SetActive(false);
        }
    }

    /// <summary>
    /// Called when the exit button on the leaderboard window is pressed
    /// </summary>
    public void ExitLeaderboard()
    {
        // Reset the text
        leaderboardText.text = "";

        // Show the title window
        titleWindow.SetActive(true);

        // Hide the leaderboard window
        leaderboardWindow.SetActive(false);
    }

    /// <summary>
    /// Called when the exit button on the settings window is pressed
    /// </summary>
    public void ExitSettings()
    {
        // Show the title window
        titleWindow.SetActive(true);

        // Hide the settings window
        settingsWindow.SetActive(false);
    }
}
