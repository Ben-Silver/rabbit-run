﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KillZone : MonoBehaviour
{
	/* Private Variables */
	private GameManager gameManager;

	// Use this for initialization
	void Start()
	{
		// If no GameManager is assigned
		if (!gameManager)
		{
			// Find the GameManager
			gameManager = FindObjectOfType<GameManager>();
		}
	}
	
	// Update is called once per frame
	void Update()
	{
		
	}

	// If the player falls off
	void OnTriggerEnter2D(Collider2D other)
	{
		// If it was the player
		if (other.gameObject.tag.Equals("Player"))
		{
			// Add the score to the leaderboard
			gameManager.leaderboard.AddToLeaderboard(Mathf.RoundToInt(gameManager.score));
			
			// Register the game as no-longer playing
			gameManager.gameIsPlaying = false;

			// Go back to the title
			SceneManager.LoadScene("Title");
		}
	}
}
