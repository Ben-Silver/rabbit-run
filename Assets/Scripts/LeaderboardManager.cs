﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardManager : MonoBehaviour
{
    /* Public Variables */
    public List<int> leaderboard;

    /* Private Variables */
    [SerializeField] private bool loadedLeaderboard = false;
    private bool isHighScore = false;

    // Start is called before the first frame update
    void Awake()
    {
        // Load the leaderboard
        LoadLeaderboard();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Adds a score to the current leaderboard
    /// </summary>
    /// <param name="score">The score being added</param>
    public void AddToLeaderboard(int score)
    {
        // Add the score to the leaderboard
        leaderboard.Add(score);

        // Save the leaderboard
        SaveLeaderboard();
    }

    /// <summary>
    /// Saves the leaderboard to PlayerPrefs
    /// </summary>
    public void SaveLeaderboard()
    {
        // Sort the leaderboard
        leaderboard.Sort();

        // Loop through the leaderboard
        for (int i = 0; i < leaderboard.Count; i++)
        {
            // Set the score at i in PlayerPrefs
            PlayerPrefs.SetInt("score_" + i, leaderboard[i]);
        }

        // Save the length of the leaderboard
        PlayerPrefs.SetInt("leaderboard_length", leaderboard.Count);
    }

    /// <summary>
    /// Loads the leaderboard from PlayerPrefs
    /// </summary>
    public void LoadLeaderboard()
    {
        // If the leaderboard has not yet been loaded
        if (!loadedLeaderboard)
        {
            // If the leaderboard contains scores
            if (PlayerPrefs.HasKey("leaderboard_length"))
            {
                // Loop through the leaderboard in PlayerPrefs
                for (int i = 0; i < PlayerPrefs.GetInt("leaderboard_length"); i++)
                {
                    // Add the score at i to the leaderboard list
                    leaderboard.Add(PlayerPrefs.GetInt("score_" + i));
                }
            }

            // Prevent the leaderboard from being loaded again
            loadedLeaderboard = true;
        }
    }

    /// <summary>
    /// Deletes the leaderboard from PlayerPrefs
    /// </summary>
    public void DeleteLeaderboard()
    {
        // If the leaderboard contains scores
        if (PlayerPrefs.HasKey("leaderboard_length"))
        {
            // Loop through the leaderboard in PlayerPrefs
            for (int i = 0; i < PlayerPrefs.GetInt("leaderboard_length"); i++)
            {
                // Delete the score at i
                PlayerPrefs.DeleteKey("score_" + i);
            }

            // Delete the leaderboard_length
            PlayerPrefs.DeleteKey("leaderboard_length");
        }

        // Register the leaderboard as unloaded
        loadedLeaderboard = false;

        // Clear the leaderboard list
        ClearLeaderboard();

        // Load the leaderboard
        LoadLeaderboard();

        // Save the leaderboard
        SaveLeaderboard();
    }

    /// <summary>
    /// Clears the leaderboard list
    /// </summary>
    public void ClearLeaderboard()
    {
        // Clear the leaderboard
        leaderboard.Clear();
    }
}
