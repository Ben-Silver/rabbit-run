﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LeaderboardManager))]
public class LeaderboardManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        LeaderboardManager leaderboard = (LeaderboardManager)target;

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Save Scores"))
        {
            leaderboard.SaveLeaderboard();
            Debug.Log("Saving scores");
        }

        if (GUILayout.Button("Load Scores"))
        {
            leaderboard.LoadLeaderboard();
            Debug.Log("Loading scores");
        }

        if (GUILayout.Button("Clear Scores"))
        {
            leaderboard.ClearLeaderboard();
            Debug.Log("Clearing scores");
        }

        GUILayout.EndHorizontal();
    }
}
