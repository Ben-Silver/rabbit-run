﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlatform : MonoBehaviour
{
	/* Public Variables */
	public GameManager gameManager;
	public GameObject platformPrefab;
	public float[] spawnPositions;

	/* Private Variables */
	private float spawnCounter = 0;
	private float spawnCounterInitial = 0.01f;
	private float spawnCounterMax = 1.4f;
	private bool initialSpawn;
	private bool needsSpawn;
	private int lastIndex;
	private int spawnIndex;

	// Use this for initialization
	void Start()
	{
		// If no GameManager is assigned
		if (!gameManager)
		{
			// Find the GameManager
			gameManager = FindObjectOfType<GameManager>();
		}
	}
	
	// Update is called once per frame
	void Update()
	{
		// If the game is playing
		if (gameManager.gameIsPlaying)
		{
			// Increase the spawn timer
			spawnCounter += Time.deltaTime;

			// If the current time has exceeded the initial spawn counter
			if (spawnCounter >= spawnCounterInitial && !initialSpawn)
			{
				// Spawn a platform
				Spawn();

				// Switch to the regular spawn timer threshold
				initialSpawn = true;
			}
			else if (spawnCounter >= spawnCounterMax && initialSpawn)
			{
				// Spawn a platform
				Spawn();
			}
		}
	}

	/// <summary>
	/// Function to spawn a platform
	/// </summary>
	public void Spawn()
	{
		if (!initialSpawn)
		{
			// Set spawnIndex to a random element in the list
			spawnIndex = Random.Range(0, spawnPositions.Length);
		}
		else
		{
			// Compare the last index to the max bounds of the positions list
			if (lastIndex == spawnPositions.Length)
			{
				// Keep the platforms within a reachable jumping distance
				spawnIndex = Random.Range(lastIndex - 1, lastIndex);
				//Debug.Log("Spawned a platform at " + spawnPositions[spawnIndex].ToString());
			}
			// Compare the last index to the min bounds of the positions list
			else if (lastIndex == 0)
			{
				// Keep the platforms within a reachable jumping distance
				spawnIndex = Random.Range(0, lastIndex + 2);
				//Debug.Log("Spawned a platform at " + spawnPositions[spawnIndex].ToString());
			}
			else
			{
				// Keep the platforms within a reachable jumping distance
				spawnIndex = Random.Range(lastIndex - 1, spawnPositions.Length);
				//Debug.Log("Spawned a platform at " + spawnPositions[spawnIndex].ToString());
			}

		}

		// Spawn a new platform at the generated height
		Instantiate(platformPrefab, new Vector2(transform.position.x, spawnPositions[spawnIndex]), transform.rotation);
		
		// Store the position of the newly spawned platform
		lastIndex = spawnIndex;

		// Reset the spawn timer
		spawnCounter = 0;
	}
}
